# Install packages #

1. First install libraries

```shell
pip install -r requirements.txt
```

2. Download https://github.com/sebastianprobst/resonator_tools

3. Go to the downloaded folder `resonator_tools` and run
```shell
python setup.py install
```

# Running #

```shell
jupyter notebook
```

and open the [`.ipynb` file](./2021-01-05_resonator-circle-fit.ipynb)
